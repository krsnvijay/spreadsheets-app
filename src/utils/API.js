const url = 'https://spreadsheets-api.herokuapp.com/api';
const defaultHeaders = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};
const headers = () => {
  const token = localStorage.getItem('token');
  return {
    ...defaultHeaders,
    'Authorization': `Bearer ${token}`
  };
}
const API = {
  auth: {
    login: (user) => fetch(`${url}/auth/login`, { method: 'post', body: JSON.stringify(user), headers: defaultHeaders }),
    register: (user) => fetch(`${url}/auth/register`, { method: 'post', body: JSON.stringify(user), headers: defaultHeaders }),
    info: () => fetch(`${url}/auth/info`, { method: 'get', headers: headers() })
  },
  sheets: {
    fetchAll: () => fetch(`${url}/sheets/`, { method: 'get', headers: headers() }),
    createOne: (sheet) => fetch(`${url}/sheets/`, { method: 'post', headers: headers(), body: JSON.stringify(sheet) }),
    shareOne: (sheetId, userId) => fetch(`${url}/sheets/${sheetId}/share`, { method: 'post', headers: headers(), body: JSON.stringify({ userId }) }),
    fetchOne: (sheetId) => fetch(`${url}/sheets/${sheetId}`, { method: 'get', headers: headers() }),
    deleteOne: (sheetId) => fetch(`${url}/sheets/${sheetId}`, { method: 'delete', headers: headers() }),
    updateOne: (sheetId, body) => fetch(`${url}/sheets/${sheetId}`, { method: 'PATCH', headers: headers(), body: JSON.stringify(body) }),
    downloadOne: (sheetId) => fetch(`${url}/sheets/${sheetId}/download`, { method: 'get', headers: headers() })

  },
  users: {
    fetchAll: () => fetch(`${url}/users/`, { method: 'get', headers: headers() }),
    fetchOne: (userId) => fetch(`${url}/users/${userId}`, { method: 'get', headers: headers() })
  }
};
export default API;