import jwt from 'jsonwebtoken';
const res2Json = (res) => res.json();
const saveToken = (token) => localStorage.setItem('token', token);
const getToken = () => localStorage.getItem('token');
const removeToken = () => localStorage.removeItem('token');
const decodeToken = (token) => jwt.decode(token);
const getUser = () => decodeToken(getToken());
const handleErrors = async (res) => {
  if (!res.ok) {
    const { message } = await res.json();
    throw Error(`${res.statusText}, ${message}`);
  }
  return res;
}
const sortNestedArray = (colIdx) => (a, b) => {
  if (a[colIdx] === b[colIdx]) {
    return 0;
  }
  else {
    return (a[colIdx] < b[colIdx]) ? -1 : 1;
  }
}

export { res2Json, saveToken, getToken, handleErrors, getUser, removeToken, sortNestedArray }