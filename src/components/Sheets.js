import React, { Component } from 'react';
import { Redirect, Link } from "react-router-dom";
import moment from 'moment';
import API from '../utils/API';
import { res2Json, getUser, handleErrors, removeToken } from '../utils/helpers';
const Sheet = ({ sheet }) => {
  return (<li>
    <Link to={`/sheets/${sheet._id}`} >{sheet.name}</Link>
    , created {moment(sheet.created).fromNow()}
  </li>);
};
const SheetsList = ({ list, title }) => {
  return (<div>
    <h2>{title}</h2>
    <hr />
    <ul>
      {list.length > 0 ? list.map(sheet => <Sheet key={sheet._id} sheet={sheet} />) :
        <li>No Sheets Found</li>
      }
    </ul>
  </div>);
}
class Sheets extends Component {
  state = {
    user: getUser(),
    sheets: [],
    sharedSheets: [],
    redirectToReferrer: null,
    loading: true,
    message: null
  }
  componentDidMount() {
    const mySheets = API.sheets.fetchAll().then(handleErrors).then(res2Json);
    const sharedSheets = API.auth.info().then(handleErrors).then(res2Json).then(user => user.sharedSheets);
    Promise.all([mySheets, sharedSheets])
      .then(([mine, shared]) => {
        this.setState({ sheets: mine, sharedSheets: shared, loading: false });
      }).catch(err => this.setState({ message: err.message, loading: false }));
  }
  createSheet = (e) => {
    e.preventDefault();
    const { name, is_public } = e.target;
    const sheet = {
      name: name.value,
      is_public: !is_public.value
    }
    API.sheets.createOne(sheet)
      .then(handleErrors)
      .then(res2Json)
      .then(data => {
        console.log(data);
        this.setState({ redirectToReferrer: `/sheets/${data._id}` });
      }).catch(err => this.setState({ message: err.message }));
  }
  handleSignOut = (e) => {
    e.preventDefault();
    removeToken();
    this.setState({ redirectToReferrer: '/' });
  }
  render() {
    const { redirectToReferrer } = this.state;
    if (redirectToReferrer !== null) {
      return <Redirect from='/sheets' to={redirectToReferrer} />;
    }
    const { sheets, loading } = this.state;
    if (loading)
      return <div className="loader" />

    return (
      <div>
        <div>
          <h1>Welcome to Spread Sheets</h1>
          Logged In as {this.state.user.username}, <a href="/" onClick={this.handleSignOut}>Sign Out?</a>
        </div>
        <br />
        <div>{this.state.message}</div>
        <br />
        <div>
          <h2>Create Sheet</h2>
          <hr />
          <form onSubmit={this.createSheet}>
            <input className="button" type="submit" value="+" />
            <input className="text-input" type="input" name="name" placeholder="Sheet Name" required />
            <label><input className="text-input" type="checkbox" name="is_public" value={true} defaultChecked />private?</label>
          </form>
        </div>
        <br />
        <SheetsList title="My Sheets" list={this.state.sheets} />
        <br />
        <SheetsList title="Shared Sheets" list={this.state.sharedSheets} />
      </div >
    );
  }
}
export default Sheets