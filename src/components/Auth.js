import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import API from '../utils/API';
import { res2Json, saveToken, handleErrors, getToken } from '../utils/helpers';
class Auth extends Component {
  state = {
    authType: true,
    redirectToReferrer: !!getToken(),
    message: null
  };
  handleSubmit = (e) => {
    e.preventDefault();
    const { username, email, password } = e.target;
    const user = {
      username: username.value,
      email: (email && email.value) || null,
      password: password.value
    };
    if (!this.state.authType) {
      API.auth.register(user)
        .then(handleErrors)
        .then(res2Json)
        .then(data => {
          saveToken(data.token);
          this.setState({ redirectToReferrer: true });
        }).catch(err => this.setState({ message: err.message }));
    }
    else {
      API.auth.login(user)
        .then(handleErrors)
        .then(res2Json)
        .then(data => {
          saveToken(data.token);
          this.setState({ redirectToReferrer: true });
        }).catch(err => this.setState({ message: err.message }));
    }


  }
  handleChange = (e) => {
    const { value } = e.target;
    console.log(value);
    this.setState({
      authType: value === 'Login'
    });
  };
  handleClick = (e) => {
    e.preventDefault();
    this.setState(prevState => ({
      authType: !prevState.authType
    }));
  };
  render() {
    const { redirectToReferrer } = this.state;
    if (redirectToReferrer) {
      return <Redirect to="/sheets" />;
    }
    return (
      <div>
        <h1>Spread Sheets</h1>
        <h2>{this.state.authType ? 'Login' : 'Register'}</h2>
        <form onSubmit={this.handleSubmit}>
          {this.state.authType ? null : <input className="text-input" type="email" name="email" placeholder="enter email" required />}
          {this.state.authType ? null : <br />}
          <input className="text-input" type="text" name="username" placeholder="enter username" required />
          <br />
          <input className="text-input" type="password" name="password" placeholder="enter password" required />
          <br />
          <div>{this.state.message}</div>
          <br />
          <div>
            {this.state.authType ? 'New User? ' : 'Existing User? '}
            <a href="/" onClick={this.handleClick}>{this.state.authType ? 'Register' : 'Login'}</a>
          </div>
          <br />
          <input className="button" type="submit" value={this.state.authType ? 'Login' : 'Register'} />
        </form>
      </div>
    );
  }
}
export default Auth;