import React, { Component } from 'react';
import API from '../utils/API';
import Link from 'react-router-dom/Link';
import { res2Json, getUser, handleErrors, sortNestedArray } from '../utils/helpers';
import moment from 'moment';
const User = ({ user, handleClick }) => {
  return <li><a role="button" href="#" onClick={handleClick} data-id={user._id}>{user.username}</a></li>
}

class ShareSheet extends Component {
  state = {
    searchText: '',
    users: [],
    sheet: {},
    loading: true,
    message: null
  }
  handleClick = (e) => {
    e.preventDefault();
    const userId = e.target.dataset.id;
    const { _id: sheetId } = this.state.sheet;
    API.sheets.shareOne(sheetId, userId)
      .then(handleErrors)
      .then(res2Json)
      .then(data => this.setState({ message: 'Sheet Shared Successfully' }))
      .catch(err => this.setState({ message: `Error Sharing Sheet,${err.message}` }));

  }
  componentDidMount() {
    const sheetId = this.props.match.params.sheetId;
    const users = API.users.fetchAll().then(handleErrors).then(res2Json);
    const sheet = API.sheets.fetchOne(sheetId).then(handleErrors).then(res2Json);
    Promise.all([
      users,
      sheet
    ]).then(data => this.setState(
      { users: data[0], sheet: data[1], loading: false }
    ))
      .catch(err => this.setState({ sheet: null, loading: false }));
  }
  handleChange = (e) => {
    this.setState({ searchText: e.target.value });
  }
  render() {
    const { sheet, users, loading, searchText, message } = this.state;
    if (sheet === null)
      return (<h1>Sheet Does Not Exist, <Link to={`/sheets/${this.props.match.params.sheetId}`}>Go Back</Link></h1>)
    if (message !== null)
      return (<h1>{message}, <Link to={`/sheets/${sheet._id}`}>Go Back</Link></h1>)
    if (loading)
      return <div className="loader" />
    const filteredUsers = (users && users.filter(user => user.username.toLowerCase().indexOf(searchText.toLowerCase()) > -1)) || []
    return (
      <div>
        <h1>Share Sheet {sheet && sheet.name}</h1>
        <input type="text" name="search" onChange={this.handleChange} placeholder="enter username" />
        <ul>
          {filteredUsers.map(user => <User key={user._id} user={user} handleClick={this.handleClick} />)}
        </ul>
      </div>
    );
  }
}
export default ShareSheet;