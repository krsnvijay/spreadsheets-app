import React, { Component } from 'react';
import moment from 'moment';
import {
  Link,
  Redirect
} from "react-router-dom";

import fileDownload from 'js-file-download';
import API from '../utils/API';
import Table from './Table';
import { res2Json, getUser, handleErrors, sortNestedArray } from '../utils/helpers';

class Sheet extends Component {
  state = {
    sheet: {},
    rowCount: 10,
    cellCount: 5,
    message: null,
    loading: true,
    is_owner: false,
    redirectToReferrer: null,
    user: getUser(),
  }
  componentDidMount() {
    const sheetId = this.props.match.params.sheetId;
    API.sheets.fetchOne(sheetId)
      .then(handleErrors)
      .then(res2Json)
      .then(async sheet => {
        const sheetdata = (sheet.data && sheet.data.length > 0 && sheet.data) || null;
        const rowCount = (sheetdata && sheetdata.length) || this.state.rowCount;
        const cellCount = (sheetdata && sheetdata[0] && sheetdata[0].length) || this.state.cellCount;
        const rows = sheetdata || Array(rowCount).fill(null).map(row => Array(cellCount).fill(null));
        const is_owner = this.state.user._id === sheet.author;
        const { username } = (is_owner && this.state.user)
          || await API.users.fetchOne(sheet.author).then(handleErrors).then(res2Json);
        const newSheet = { ...sheet, data: rows, username };
        this.setState({ sheet: newSheet, rowCount, cellCount, loading: false, is_owner });
      }).catch(err => this.setState({ sheet: null, loading: false, message: err.message }));
  }
  handleCellChange = (value, rowIndex, cellIndex) => {
    this.setState(prevState => {
      const copy = [...prevState.sheet.data].map(arr => [...arr]);
      copy[rowIndex][cellIndex] = value;
      const newSheet = { ...prevState.sheet, data: copy };
      return { sheet: newSheet };
    });
  }
  sortByColumn = (colIndex, rev = false) => {
    this.setState(({ sheet }) => {
      let copy = [...sheet.data].map(arr => [...arr]);
      copy = copy.sort(sortNestedArray(colIndex));
      if (rev)
        copy = copy.reverse();
      const newSheet = { ...sheet, data: copy };
      return { sheet: newSheet };
    });
  }
  addNewRow = () => {
    this.setState(prevState => {
      const copy = [...prevState.sheet.data].map(arr => [...arr]);
      const row = Array(prevState.cellCount).fill(null);
      copy.push(row);
      const newSheet = { ...prevState.sheet, data: copy };
      return { sheet: newSheet, rowCount: prevState.rowCount + 1 }
    });
  }
  addNewColumn = () => {
    this.setState(prevState => {
      const copy = [...prevState.sheet.data].map(arr => [...arr, null]);
      const newSheet = { ...prevState.sheet, data: copy };
      return { sheet: newSheet, cellCount: prevState.cellCount + 1 };
    });
  }
  saveSheet = (e) => {
    e.preventDefault();
    const { sheet } = this.state;
    API.sheets.updateOne(sheet._id, { data: sheet.data })
      .then(handleErrors)
      .then(res2Json)
      .then(data => {
        this.setState({ message: 'Saved Successfully' });
      })
      .catch(err => this.setState({ message: err.message }));
  }
  changeSheetVisibility = (e) => {
    e.preventDefault();
    const { sheet } = this.state;
    API.sheets.updateOne(sheet._id, { is_public: !sheet.is_public })
      .then(handleErrors)
      .then(res2Json)
      .then(data => {
        this.setState({ sheet: data, message: 'Visibility Changed Successfully' });
      }).catch(err => this.setState({ message: err.message }));
  }
  deleteSheet = (e) => {
    e.preventDefault();
    const { sheet } = this.state;
    API.sheets.deleteOne(sheet._id)
      .then(handleErrors)
      .then(res2Json)
      .then(data => {
        this.setState({ message: 'Sheet Deleted Successfully' });
      }).catch(err => this.setState({ message: err.message }));
  }
  downloadSheet = (e) => {
    e.preventDefault();
    const { sheet } = this.state;
    API.sheets.downloadOne(sheet._id)
      .then(handleErrors)
      .then(async res => await res.blob())
      .then(data => {
        fileDownload(data, `${sheet.name}.csv`);
        this.setState({ message: 'Sheet Downloaded Successfully' });
      }).catch(err => this.setState({ message: err.message }));
  }
  shareSheet = (e) => this.setState(({ sheet }) => ({ redirectToReferrer: `/sheets/${sheet._id}/share` }));

  render() {
    const { sheet, rowCount, cellCount, loading, is_owner, redirectToReferrer } = this.state;
    if (sheet === null)
      return (<h1>Sheet Does Not Exist, <Link to="/sheets">Go Back</Link></h1>)
    if (redirectToReferrer !== null) {
      return <Redirect from={`/sheets/${sheet._id}`} to={redirectToReferrer} />;
    }
    if (loading)
      return <div className="loader" />
    else
      return (
        <div>
          <div>
            <h1>Spread Sheet - {sheet.name}</h1>
            <h2>Created by {sheet.username}, {moment(sheet.created).fromNow()}</h2>
          </div>
          <div>{this.state.message}</div>
          <div>
            {is_owner && <input className="button" type="button" onClick={this.saveSheet} value="Save" />}
            {is_owner && <input className="button" type="button" onClick={this.deleteSheet} value="Delete" />}
            {is_owner && <input className="button" type="button" onClick={this.shareSheet} value="Share" />}
            {is_owner && <input className="button" type="button" onClick={this.changeSheetVisibility} value={`Make ${sheet.is_public === true ? 'Private' : 'Public'}`} />}
            <input type="button" className="button" onClick={this.downloadSheet} value="Download" />
          </div>
          <Table data={sheet.data || []} rowCount={rowCount} cellCount={cellCount}
            addNewColumn={this.addNewColumn} addNewRow={this.addNewRow} handleCellChange={this.handleCellChange} sortByColumn={this.sortByColumn}
          />
        </div >
      );
  }
}
export default Sheet