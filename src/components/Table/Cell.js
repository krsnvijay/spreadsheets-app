import React, { Component } from 'react';

export default class Cell extends Component {


  handleCellChange = (e) => {
    const { rowIndex, cellIndex, onCellChange } = this.props;
    const text = e.target.value.trim() || null;
    onCellChange(text, rowIndex, cellIndex);
  }

  render() {
    const { value } = this.props
    return (
      <td>
        <input className="cell" type="text" value={value || ''} onChange={this.handleCellChange} />
      </td>
    )
  }
}