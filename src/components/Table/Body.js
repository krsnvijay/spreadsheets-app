import React from 'react';
import Row from './Row';

export default function Body(props) {
  const { rows, onCellChange, addNewRow } = props;
  return (
    <tbody>
      {rows.map((row, i) => <Row columns={row} key={i} rowIndex={i} onCellChange={onCellChange} />)}
      <tr><td><input type="button" className="button" value="+" onClick={addNewRow} /></td></tr>
    </tbody>
  );
}