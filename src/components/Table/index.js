import React from 'react';
import Head from './Head';
import Body from './Body';

export default function Table(props) {
  const { data, rowCount, cellCount, addNewColumn, handleCellChange, addNewRow, sortByColumn } = props;
  const headers = [..."ABCDEFGHIJKLMNOPQRSTUVWXYZ".slice(0, cellCount)];
  return (
    <table style={{ tableLayout: 'fixed' }} cellSpacing={0}>
      <Head headers={headers} addNewColumn={addNewColumn} sortByColumn={sortByColumn} />
      <Body rows={data} onCellChange={handleCellChange} addNewRow={addNewRow} />
    </table >
  );
}
