import React from 'react';
import Cell from './Cell';

export default function Row(props) {
  const { rowIndex, columns, onCellChange } = props;
  return (
    <tr>
      <td>{rowIndex}</td>
      {columns.map((column, i) => <Cell value={column} key={i} rowIndex={rowIndex} cellIndex={i} onCellChange={onCellChange} />)}
    </tr>
  )
}