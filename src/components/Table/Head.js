import React, { Component } from 'react';
import { sortNestedArray } from '../../utils/helpers';
const Arrow = (order) => {
  if (order)
    return '↑';
  else if (order === null)
    return null;
  return '↓';
};
export default class Head extends Component {
  state = {
    order: null,
    index: null,
  }
  handleClick = (e) => {
    const colIndex = this.props.headers.indexOf(e.target.innerText);
    this.setState(prevState => {
      let { order } = prevState;
      this.props.sortByColumn(colIndex, !order);
      return { order: !order, index: colIndex };
    });
  };
  handleBlur = (e) => {
    this.setState({ order: null });
  }
  render() {
    const { headers, addNewColumn } = this.props;
    return (
      <thead>
        <tr>
          <th style={{ width: '100px' }}></th>
          {headers.map((header, i) => <th key={i} style={{ width: '100px' }} onBlur={this.handleBlur}><span onClick={this.handleClick}>{header}</span>{this.state.index === i && Arrow(this.state.order)}</th>)}
          <th><input type="button" className="button" value="+" onClick={addNewColumn} /></th>
        </tr>
      </thead>
    )
  }
}