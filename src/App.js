import React from "react";
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect,
} from "react-router-dom";
import Auth from './components/Auth';
import Sheets from './components/Sheets';
import Sheet from './components/Sheet';
import ShareSheet from './components/ShareSheet';

const App = () => (
  <Router>
    <div className="App">
      <Switch>
        <Route exact path="/" component={Auth} />
        <PrivateRoute exact path="/sheets" component={Sheets} />
        <PrivateRoute exact path="/sheets/:sheetId" component={Sheet} />
        <PrivateRoute exact path="/sheets/:sheetId/share" component={ShareSheet} />
        <Route component={() => <h1>Page Does Not Exist, <Link to="/">Go Back</Link></h1>} />
      </Switch>
    </div>
  </Router>
);

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem('token') ? (
        <Component {...props} />
      ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location }
            }}
          />
        )
    }
  />
);
export default App;